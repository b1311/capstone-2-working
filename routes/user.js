const express = require('express');
const router = express.Router();
const userController = require('../controllers/user')
const auth = require('../auth');


// Route for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))

})

//Route for User Authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})

// Set User as Admin
router.post('/setUserAsAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.setUserAsAdmin(req.body.email).then(resultFromController => 
		res.send(resultFromController))
})


module.exports = router;