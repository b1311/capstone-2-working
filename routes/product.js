const express = require('express');
const router = express.Router();
const productController = require("../controllers/product")
const auth = require("../auth");


// Create Product

router.post('/create', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.createProduct(req.body).then(resultFromController =>
		res.send(resultFromController))

})

// Retrieve all active Products

router.get('/', (req, res) => {
	productController.retrieveAllActive().then(resultFromController => 
		res.send(resultFromController));
})

// Retrieve Single Product

router.get('/:productId', (req, res) => {

	productController.singleProduct(req.params.productId).then(resultFromController => 
		res.send(resultFromController))
})

// Update product

router.put('/:productId/update', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
})


//Archive Product

router.post('/:productId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.archiveProduct(req.body.isActive, req.params.productId).then(resultFromController => 
		res.send(resultFromController))
})



module.exports = router;