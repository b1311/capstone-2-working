const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, 'Total Amount']
	},

	purchasedOn :{
		type: Date,
		default: new Date()
	},
	

})