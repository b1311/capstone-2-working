const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		birthday: reqBody.birthday,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
		
	})

	return newUser.save().then((user, error) =>{
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//User login

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)	
			
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})
}


// Set User as Admin


module.exports.setUserAsAdmin = (reqBodyEmail) => {

	return User.findOne({email: reqBodyEmail}).then(result => {
		let myquery = {email: result.email};
		let newvalue = { $set: {isAdmin: true}};
		
		if (result.isAdmin == false){
			User.updateOne(myquery, newvalue, function (error, res){
				if (error) {
					return error;
				} else {
					return true;
				}
			})
		}


	})

	// if(isAdmin == true){
	// 	return false
	// } else {

	// }
}

